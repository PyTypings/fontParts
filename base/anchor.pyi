from typing import Optional, Tuple

from _typeshed import Incomplete

from fontParts.base import normalizers as normalizers
from fontParts.base.base import BaseObject as BaseObject
from fontParts.base.base import IdentifierMixin as IdentifierMixin
from fontParts.base.base import InterpolationMixin as InterpolationMixin
from fontParts.base.base import PointPositionMixin as PointPositionMixin
from fontParts.base.base import SelectionMixin as SelectionMixin
from fontParts.base.base import TransformationMixin as TransformationMixin
from fontParts.base.base import dynamicProperty as dynamicProperty
from fontParts.base.base import reference as reference
from fontParts.base.color import Color as Color
from fontParts.base.compatibility import (
    AnchorCompatibilityReporter as AnchorCompatibilityReporter,
)
from fontParts.base.deprecated import DeprecatedAnchor as DeprecatedAnchor
from fontParts.base.deprecated import RemovedAnchor as RemovedAnchor
from fontParts.base.font import BaseFont
from fontParts.base.glyph import BaseGlyph
from fontParts.base.layer import BaseLayer

class BaseAnchor(
    BaseObject,
    TransformationMixin,
    DeprecatedAnchor,
    RemovedAnchor,
    PointPositionMixin,
    InterpolationMixin,
    SelectionMixin,
    IdentifierMixin,
):
    copyAttributes: Tuple[str, ...]
    glyph: Optional[BaseGlyph]
    layer: Optional[BaseLayer]
    font: Optional[BaseFont]
    x: float
    y: float
    index: Optional[int]
    name: Optional[str]
    color: Optional[Color]
    compatibilityReporterClass = AnchorCompatibilityReporter
    def isCompatible(self, other): ...
    def round(self) -> None: ...
