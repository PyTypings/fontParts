from _typeshed import Incomplete
from fontParts.base import normalizers as normalizers
from fontParts.base.base import BaseObject as BaseObject, IdentifierMixin as IdentifierMixin, SelectionMixin as SelectionMixin, TransformationMixin as TransformationMixin, dynamicProperty as dynamicProperty, reference as reference
from fontParts.base.deprecated import DeprecatedBPoint as DeprecatedBPoint, RemovedBPoint as RemovedBPoint
from fontParts.base.errors import FontPartsError as FontPartsError

class BaseBPoint(BaseObject, TransformationMixin, SelectionMixin, DeprecatedBPoint, IdentifierMixin, RemovedBPoint):
    def __eq__(self, other): ...
    __hash__: Incomplete
    contour: Incomplete
    glyph: Incomplete
    layer: Incomplete
    font: Incomplete
    anchor: Incomplete
    bcpIn: Incomplete
    bcpOut: Incomplete
    type: Incomplete
    index: Incomplete
    def round(self) -> None: ...

def relativeBCPIn(anchor, BCPIn): ...
def absoluteBCPIn(anchor, BCPIn): ...
def relativeBCPOut(anchor, BCPOut): ...
def absoluteBCPOut(anchor, BCPOut): ...
