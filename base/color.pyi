from _typeshed import Incomplete

class Color(tuple):
    r: Incomplete
    g: Incomplete
    b: Incomplete
    a: Incomplete
