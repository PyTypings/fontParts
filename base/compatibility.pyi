from _typeshed import Incomplete
from fontParts.base.base import dynamicProperty as dynamicProperty

class BaseCompatibilityReporter:
    objectName: str
    def __init__(self, obj1, obj2) -> None: ...
    fatal: bool
    warning: bool
    title: Incomplete
    object1: Incomplete
    object1Name: Incomplete
    object2: Incomplete
    object2Name: Incomplete
    def report(self, showOK: bool = ..., showWarnings: bool = ...) -> None: ...
    def formatFatalString(self, text): ...
    def formatWarningString(self, text): ...
    def formatOKString(self, text): ...
    @staticmethod
    def reportSubObjects(reporters, showOK: bool = ..., showWarnings: bool = ...): ...
    @staticmethod
    def reportCountDifference(subObjectName, object1Name, object1Count, object2Name, object2Count): ...
    @staticmethod
    def reportOrderDifference(subObjectName, object1Name, object1Order, object2Name, object2Order): ...
    @staticmethod
    def reportDifferences(object1Name, subObjectName, subObjectID, object2Name): ...

class FontCompatibilityReporter(BaseCompatibilityReporter):
    objectName: str
    guidelineCountDifference: bool
    layerCountDifference: bool
    guidelinesMissingFromFont2: Incomplete
    guidelinesMissingInFont1: Incomplete
    layersMissingFromFont2: Incomplete
    layersMissingInFont1: Incomplete
    layers: Incomplete
    def __init__(self, font1, font2) -> None: ...
    font1: Incomplete
    font1Name: Incomplete
    font2: Incomplete
    font2Name: Incomplete
    def report(self, showOK: bool = ..., showWarnings: bool = ...): ...

class LayerCompatibilityReporter(BaseCompatibilityReporter):
    objectName: str
    glyphCountDifference: bool
    glyphsMissingFromLayer2: Incomplete
    glyphsMissingInLayer1: Incomplete
    glyphs: Incomplete
    def __init__(self, layer1, layer2) -> None: ...
    layer1: Incomplete
    layer1Name: Incomplete
    layer2: Incomplete
    layer2Name: Incomplete
    def report(self, showOK: bool = ..., showWarnings: bool = ...): ...

class GlyphCompatibilityReporter(BaseCompatibilityReporter):
    objectName: str
    contourCountDifference: bool
    componentCountDifference: bool
    guidelineCountDifference: bool
    anchorDifferences: Incomplete
    anchorCountDifference: bool
    anchorOrderDifference: bool
    anchorsMissingFromGlyph1: Incomplete
    anchorsMissingFromGlyph2: Incomplete
    componentDifferences: Incomplete
    componentOrderDifference: bool
    componentsMissingFromGlyph1: Incomplete
    componentsMissingFromGlyph2: Incomplete
    guidelinesMissingFromGlyph1: Incomplete
    guidelinesMissingFromGlyph2: Incomplete
    contours: Incomplete
    def __init__(self, glyph1, glyph2) -> None: ...
    glyph1: Incomplete
    glyph1Name: Incomplete
    glyph2: Incomplete
    glyph2Name: Incomplete
    def report(self, showOK: bool = ..., showWarnings: bool = ...): ...

class ContourCompatibilityReporter(BaseCompatibilityReporter):
    objectName: str
    openDifference: bool
    directionDifference: bool
    segmentCountDifference: bool
    segments: Incomplete
    def __init__(self, contour1, contour2) -> None: ...
    contour1: Incomplete
    contour1Name: Incomplete
    contour2: Incomplete
    contour2Name: Incomplete
    def report(self, showOK: bool = ..., showWarnings: bool = ...): ...

class SegmentCompatibilityReporter(BaseCompatibilityReporter):
    objectName: str
    typeDifference: bool
    def __init__(self, contour1, contour2) -> None: ...
    segment1: Incomplete
    segment1Name: Incomplete
    segment2: Incomplete
    segment2Name: Incomplete
    def report(self, showOK: bool = ..., showWarnings: bool = ...): ...

class ComponentCompatibilityReporter(BaseCompatibilityReporter):
    objectName: str
    baseDifference: bool
    def __init__(self, component1, component2) -> None: ...
    component1: Incomplete
    component1Name: Incomplete
    component2: Incomplete
    component2Name: Incomplete
    def report(self, showOK: bool = ..., showWarnings: bool = ...): ...

class AnchorCompatibilityReporter(BaseCompatibilityReporter):
    objectName: str
    nameDifference: bool
    def __init__(self, anchor1, anchor2) -> None: ...
    anchor1: Incomplete
    anchor1Name: Incomplete
    anchor2: Incomplete
    anchor2Name: Incomplete
    def report(self, showOK: bool = ..., showWarnings: bool = ...): ...

class GuidelineCompatibilityReporter(BaseCompatibilityReporter):
    objectName: str
    nameDifference: bool
    def __init__(self, guideline1, guideline2) -> None: ...
    guideline1: Incomplete
    guideline1Name: Incomplete
    guideline2: Incomplete
    guideline2Name: Incomplete
    def report(self, showOK: bool = ..., showWarnings: bool = ...): ...
