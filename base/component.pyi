from _typeshed import Incomplete
from fontParts.base import normalizers as normalizers
from fontParts.base.base import BaseObject as BaseObject, IdentifierMixin as IdentifierMixin, InterpolationMixin as InterpolationMixin, PointPositionMixin as PointPositionMixin, SelectionMixin as SelectionMixin, TransformationMixin as TransformationMixin, dynamicProperty as dynamicProperty, reference as reference
from fontParts.base.compatibility import ComponentCompatibilityReporter as ComponentCompatibilityReporter
from fontParts.base.deprecated import DeprecatedComponent as DeprecatedComponent, RemovedComponent as RemovedComponent
from fontParts.base.errors import FontPartsError as FontPartsError

class BaseComponent(BaseObject, TransformationMixin, InterpolationMixin, SelectionMixin, IdentifierMixin, DeprecatedComponent, RemovedComponent):
    copyAttributes: Incomplete
    glyph: Incomplete
    layer: Incomplete
    font: Incomplete
    baseGlyph: Incomplete
    transformation: Incomplete
    offset: Incomplete
    scale: Incomplete
    index: Incomplete
    def draw(self, pen) -> None: ...
    def drawPoints(self, pen) -> None: ...
    def round(self) -> None: ...
    def decompose(self) -> None: ...
    compatibilityReporterClass = ComponentCompatibilityReporter
    def isCompatible(self, other): ...
    def pointInside(self, point): ...
    bounds: Incomplete
