from _typeshed import Incomplete

from fontParts.base import normalizers as normalizers
from fontParts.base.base import BaseObject as BaseObject
from fontParts.base.base import IdentifierMixin as IdentifierMixin
from fontParts.base.base import InterpolationMixin as InterpolationMixin
from fontParts.base.base import SelectionMixin as SelectionMixin
from fontParts.base.base import TransformationMixin as TransformationMixin
from fontParts.base.base import dynamicProperty as dynamicProperty
from fontParts.base.base import reference as reference
from fontParts.base.bPoint import absoluteBCPIn as absoluteBCPIn
from fontParts.base.bPoint import absoluteBCPOut as absoluteBCPOut
from fontParts.base.compatibility import (
    ContourCompatibilityReporter as ContourCompatibilityReporter,
)
from fontParts.base.deprecated import DeprecatedContour as DeprecatedContour
from fontParts.base.deprecated import RemovedContour as RemovedContour
from fontParts.base.errors import FontPartsError as FontPartsError

class BaseContour(
    BaseObject,
    TransformationMixin,
    InterpolationMixin,
    SelectionMixin,
    IdentifierMixin,
    DeprecatedContour,
    RemovedContour,
):
    segmentClass: Incomplete
    bPointClass: Incomplete
    def copyData(self, source) -> None: ...
    glyph: Incomplete
    font: Incomplete
    layer: Incomplete
    index: Incomplete
    def getIdentifierForPoint(self, point): ...
    def draw(self, pen) -> None: ...
    def drawPoints(self, pen) -> None: ...
    def autoStartSegment(self) -> None: ...
    def round(self) -> None: ...
    compatibilityReporterClass = ContourCompatibilityReporter
    def isCompatible(self, other): ...
    open: Incomplete
    clockwise: Incomplete
    def reverse(self) -> None: ...
    def pointInside(self, point): ...
    def contourInside(self, otherContour): ...
    bounds: Incomplete
    area: Incomplete
    segments: Incomplete
    def __getitem__(self, index): ...
    def __iter__(self): ...
    def __len__(self) -> int: ...
    def appendSegment(
        self,
        type: Incomplete | None = ...,
        points: Incomplete | None = ...,
        smooth: bool = ...,
        segment: Incomplete | None = ...,
    ) -> None: ...
    def insertSegment(
        self,
        index,
        type: Incomplete | None = ...,
        points: Incomplete | None = ...,
        smooth: bool = ...,
        segment: Incomplete | None = ...,
    ) -> None: ...
    def removeSegment(self, segment, preserveCurve: bool = ...) -> None: ...
    def setStartSegment(self, segment) -> None: ...
    bPoints: Incomplete
    def appendBPoint(
        self,
        type: Incomplete | None = ...,
        anchor: Incomplete | None = ...,
        bcpIn: Incomplete | None = ...,
        bcpOut: Incomplete | None = ...,
        bPoint: Incomplete | None = ...,
    ) -> None: ...
    def insertBPoint(
        self,
        index,
        type: Incomplete | None = ...,
        anchor: Incomplete | None = ...,
        bcpIn: Incomplete | None = ...,
        bcpOut: Incomplete | None = ...,
        bPoint: Incomplete | None = ...,
    ) -> None: ...
    def removeBPoint(self, bPoint) -> None: ...
    points: Incomplete
    def appendPoint(
        self,
        position: Incomplete | None = ...,
        type: str = ...,
        smooth: bool = ...,
        name: Incomplete | None = ...,
        identifier: Incomplete | None = ...,
        point: Incomplete | None = ...,
    ) -> None: ...
    def insertPoint(
        self,
        index:int,
        position: Incomplete | None = ...,
        type: str = ...,
        smooth: bool = ...,
        name: str | None = ...,
        identifier: Incomplete | None = ...,
        point: Incomplete | None = ...,
    ) -> None: ...
    def removePoint(self, point, preserveCurve: bool = ...) -> None: ...
    def setStartPoint(self, point) -> None: ...
    selectedSegments: Incomplete
    selectedPoints: Incomplete
    selectedBPoints: Incomplete
