from _typeshed import Incomplete
from fontParts.base import normalizers as normalizers
from fontParts.base.base import BaseObject as BaseObject, dynamicProperty as dynamicProperty, reference as reference
from fontParts.base.deprecated import DeprecatedFeatures as DeprecatedFeatures, RemovedFeatures as RemovedFeatures

class BaseFeatures(BaseObject, DeprecatedFeatures, RemovedFeatures):
    copyAttributes: Incomplete
    font: Incomplete
    text: Incomplete
