from __future__ import annotations

from typing import Optional, Tuple

from _typeshed import Incomplete

from fontParts.base import normalizers as normalizers
from fontParts.base.anchor import BaseAnchor
from fontParts.base.base import BaseObject as BaseObject
from fontParts.base.base import FuzzyNumber as FuzzyNumber
from fontParts.base.base import InterpolationMixin as InterpolationMixin
from fontParts.base.base import SelectionMixin as SelectionMixin
from fontParts.base.base import TransformationMixin as TransformationMixin
from fontParts.base.base import dynamicProperty as dynamicProperty
from fontParts.base.base import interpolate as interpolate
from fontParts.base.color import Color as Color
from fontParts.base.compatibility import (
    GlyphCompatibilityReporter as GlyphCompatibilityReporter,
)
from fontParts.base.component import BaseComponent
from fontParts.base.contour import BaseContour
from fontParts.base.deprecated import DeprecatedGlyph as DeprecatedGlyph
from fontParts.base.deprecated import RemovedGlyph as RemovedGlyph
from fontParts.base.errors import FontPartsError as FontPartsError
from fontParts.base.font import BaseFont
from fontParts.base.guideline import BaseGuideline
from fontParts.base.image import BaseImage
from fontParts.base.layer import BaseLayer
from fontParts.base.lib import BaseLib

class BaseGlyph(
    BaseObject,
    TransformationMixin,
    InterpolationMixin,
    SelectionMixin,
    DeprecatedGlyph,
    RemovedGlyph,
):
    copyAttributes: Tuple[str, ...]
    layer: Optional[BaseLayer]
    font: Optional[BaseFont]
    name: Optional[str]
    unicodes: Tuple[int, ...]
    unicode: Optional[int]
    width: float
    leftMargin: Optional[float]
    rightMargin: Optional[float]
    height: float
    bottomMargin: Optional[float]
    topMargin: Optional[float]
    contours: Tuple[BaseContour, ...]
    components: Tuple[BaseComponent, ...]
    anchors: Tuple[BaseAnchor, ...]
    guidelines: Tuple[BaseGuideline, ...]
    compatibilityReporterClass = GlyphCompatibilityReporter
    markColor: Optional[Color]
    note: Optional[str]
    lib: BaseLib
    tempLib: BaseLib
    image: Optional[BaseImage]
    bounds: Optional[Tuple[float, float, float, float]]
    area: float
    layers: Tuple[BaseLayer, ...]
    selectedContours: Tuple[BaseContour, ...]
    selectedComponents: Tuple[BaseComponent, ...]
    selectedAnchors: Tuple[BaseAnchor, ...]
    selectedGuidelines: Tuple[BaseGuideline, ...]
    def autoUnicodes(self) -> None: ...
    def copy(self) -> BaseGlyph: ...
    def copyData(self, source) -> None: ...
    def getPen(self) -> None: ...
    def getPointPen(self) -> None: ...
    def draw(self, pen, contours: bool = ..., components: bool = ...) -> None: ...
    def drawPoints(self, pen, contours: bool = ..., components: bool = ...) -> None: ...
    def clear(
        self,
        contours: bool = ...,
        components: bool = ...,
        anchors: bool = ...,
        guidelines: bool = ...,
        image: bool = ...,
    ) -> None: ...
    def appendGlyph(self, other, offset: Incomplete | None = ...) -> None: ...
    def __len__(self) -> int: ...
    def __iter__(self): ...
    def __getitem__(self, index: int) -> BaseContour: ...
    def appendContour(self, contour: BaseContour, offset: Incomplete | None = ...): ...
    def removeContour(self, contour: BaseContour) -> None: ...
    def clearContours(self) -> None: ...
    def removeOverlap(self) -> None: ...
    def appendComponent(
        self,
        baseGlyph: str | None = ...,
        offset: Incomplete | None = ...,
        scale: Incomplete | None = ...,
        component: BaseComponent | None = ...,
    ) -> BaseComponent: ...
    def removeComponent(self, component) -> None: ...
    def clearComponents(self) -> None: ...
    def decompose(self) -> None: ...
    def appendAnchor(
        self,
        name: str | None = ...,
        position: Incomplete | None = ...,
        color: Color | None = ...,
        anchor: BaseAnchor | None = ...,
    ) -> BaseAnchor: ...
    def removeAnchor(self, anchor) -> None: ...
    def clearAnchors(self) -> None: ...
    def appendGuideline(
        self,
        position: Incomplete | None = ...,
        angle: Incomplete | None = ...,
        name: str | None = ...,
        color: Color | None = ...,
        guideline: BaseGuideline | None = ...,
    ) -> BaseGuideline: ...
    def removeGuideline(self, guideline) -> None: ...
    def clearGuidelines(self) -> None: ...
    def round(self) -> None: ...
    def correctDirection(self, trueType: bool = ...) -> None: ...
    def autoContourOrder(self) -> None: ...
    def scaleBy(
        self,
        value,
        origin: Incomplete | None = ...,
        width: bool = ...,
        height: bool = ...,
    ) -> None: ...
    def toMathGlyph(self, scaleComponentTransform: bool = ..., strict: bool = ...): ...
    def fromMathGlyph(self, mathGlyph, filterRedundantPoints: bool = ...): ...
    def __mul__(self, factor): ...
    __rmul__ = __mul__
    def __truediv__(self, factor): ...
    __div__ = __truediv__
    def __add__(self, other): ...
    def __sub__(self, other): ...
    def interpolate(
        self,
        factor: float,
        minGlyph: BaseGlyph,
        maxGlyph: BaseGlyph,
        round: bool = ...,
        suppressError: bool = ...,
    ) -> None: ...
    def isCompatible(self, other) -> bool: ...
    def pointInside(self, point) -> bool: ...
    def getLayer(self, name: str): ...
    def newLayer(self, name: str): ...
    def removeLayer(self, layer) -> None: ...
    def addImage(
        self,
        path: Incomplete | None = ...,
        data: Incomplete | None = ...,
        scale: Incomplete | None = ...,
        position: Incomplete | None = ...,
        color: Color | None = ...,
    ): ...
    def clearImage(self) -> None: ...
    def isEmpty(self) -> bool: ...
    def loadFromGLIF(self, glifData) -> None: ...
    def dumpToGLIF(self, glyphFormatVersion: int = ...): ...
