from _typeshed import Incomplete
from fontParts.base import normalizers as normalizers
from fontParts.base.base import BaseObject as BaseObject, IdentifierMixin as IdentifierMixin, InterpolationMixin as InterpolationMixin, PointPositionMixin as PointPositionMixin, SelectionMixin as SelectionMixin, TransformationMixin as TransformationMixin, dynamicProperty as dynamicProperty, reference as reference
from fontParts.base.color import Color as Color
from fontParts.base.compatibility import GuidelineCompatibilityReporter as GuidelineCompatibilityReporter
from fontParts.base.deprecated import DeprecatedGuideline as DeprecatedGuideline, RemovedGuideline as RemovedGuideline

class BaseGuideline(BaseObject, TransformationMixin, DeprecatedGuideline, RemovedGuideline, PointPositionMixin, InterpolationMixin, IdentifierMixin, SelectionMixin):
    copyAttributes: Incomplete
    glyph: Incomplete
    layer: Incomplete
    font: Incomplete
    x: Incomplete
    y: Incomplete
    angle: Incomplete
    index: Incomplete
    name: Incomplete
    color: Incomplete
    compatibilityReporterClass = GuidelineCompatibilityReporter
    def isCompatible(self, other): ...
    def round(self) -> None: ...
