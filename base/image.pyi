from _typeshed import Incomplete
from fontParts.base import normalizers as normalizers
from fontParts.base.base import BaseObject as BaseObject, PointPositionMixin as PointPositionMixin, SelectionMixin as SelectionMixin, TransformationMixin as TransformationMixin, dynamicProperty as dynamicProperty, reference as reference
from fontParts.base.color import Color as Color
from fontParts.base.deprecated import DeprecatedImage as DeprecatedImage, RemovedImage as RemovedImage

class BaseImage(BaseObject, TransformationMixin, PointPositionMixin, SelectionMixin, DeprecatedImage, RemovedImage):
    copyAttributes: Incomplete
    def __bool__(self) -> bool: ...
    __nonzero__ = __bool__
    glyph: Incomplete
    layer: Incomplete
    font: Incomplete
    transformation: Incomplete
    offset: Incomplete
    scale: Incomplete
    color: Incomplete
    data: Incomplete
    def round(self) -> None: ...
