from typing import Tuple, Optional
from _typeshed import Incomplete
from fontParts.base import normalizers as normalizers
from fontParts.base.base import BaseObject as BaseObject, dynamicProperty as dynamicProperty, interpolate as interpolate, reference as reference
from fontParts.base.deprecated import DeprecatedInfo as DeprecatedInfo, RemovedInfo as RemovedInfo
from fontParts.base.errors import FontPartsError as FontPartsError
from fontParts.base.font import BaseFont

class BaseInfo(BaseObject, DeprecatedInfo, RemovedInfo):
    copyAttributes: Tuple[str, ...]
    font: BaseFont
    familyName: Optional[str]
    openTypeNameCompatibleFullName: Optional[str]
    openTypeNameDescription: Optional[str]
    openTypeNameDesigner: Optional[str]
    openTypeNameDesignerURL: Optional[str]
    openTypeNameLicense: Optional[str]
    openTypeNameLicenseURL: Optional[str]
    openTypeNameManufacturer: Optional[str]
    openTypeNameManufacturerURL: Optional[str]
    openTypeNamePreferredFamilyName: Optional[str]
    openTypeNamePreferredSubfamilyName: Optional[str]
    openTypeNameSampleText: Optional[str]
    openTypeNameUniqueID: Optional[str]
    openTypeNameVersion: Optional[str]
    openTypeNameWWSFamilyName: Optional[str]
    openTypeNameWWSSubfamilyName: Optional[str]

    def __hasattr__(self, attr): ...
    def __getattribute__(self, attr): ...
    def __setattr__(self, attr, value): ...
    def round(self) -> None: ...
    def update(self, other) -> None: ...
    def toMathInfo(self, guidelines: bool = ...): ...
    def fromMathInfo(self, mathInfo, guidelines: bool = ...): ...
    def interpolate(self, factor, minInfo, maxInfo, round: bool = ..., suppressError: bool = ...) -> None: ...
