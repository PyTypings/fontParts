from typing import Optional, Tuple

from fontParts.base import normalizers as normalizers
from fontParts.base.base import BaseObject as BaseObject
from fontParts.base.base import IdentifierMixin as IdentifierMixin
from fontParts.base.base import PointPositionMixin as PointPositionMixin
from fontParts.base.base import SelectionMixin as SelectionMixin
from fontParts.base.base import TransformationMixin as TransformationMixin
from fontParts.base.base import dynamicProperty as dynamicProperty
from fontParts.base.base import reference as reference
from fontParts.base.deprecated import DeprecatedPoint as DeprecatedPoint
from fontParts.base.deprecated import RemovedPoint as RemovedPoint
from fontParts.base.font import BaseFont
from fontParts.base.glyph import BaseGlyph
from fontParts.base.layer import BaseLayer
from fontParts.base.contour import BaseContour

class BasePoint(
    BaseObject,
    TransformationMixin,
    PointPositionMixin,
    SelectionMixin,
    IdentifierMixin,
    DeprecatedPoint,
    RemovedPoint,
):
    copyAttributes: Tuple[str, ...]
    contour: Optional[BaseContour]
    glyph: Optional[BaseGlyph]
    layer: Optional[BaseLayer]
    font: Optional[BaseFont]
    type: str
    smooth: bool
    x: float
    y: float
    index: Optional[int]
    name: Optional[str]
    def round(self) -> None: ...
