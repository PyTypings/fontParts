from _typeshed import Incomplete
from fontParts.base import normalizers as normalizers
from fontParts.base.base import BaseObject as BaseObject, InterpolationMixin as InterpolationMixin, SelectionMixin as SelectionMixin, TransformationMixin as TransformationMixin, dynamicProperty as dynamicProperty, reference as reference
from fontParts.base.compatibility import SegmentCompatibilityReporter as SegmentCompatibilityReporter
from fontParts.base.deprecated import DeprecatedSegment as DeprecatedSegment, RemovedSegment as RemovedSegment
from fontParts.base.errors import FontPartsError as FontPartsError

class BaseSegment(BaseObject, TransformationMixin, InterpolationMixin, SelectionMixin, DeprecatedSegment, RemovedSegment):
    __hash__: Incomplete
    contour: Incomplete
    glyph: Incomplete
    layer: Incomplete
    font: Incomplete
    def __eq__(self, other): ...
    index: Incomplete
    type: Incomplete
    smooth: Incomplete
    def __getitem__(self, index): ...
    def __iter__(self): ...
    def __len__(self) -> int: ...
    points: Incomplete
    onCurve: Incomplete
    offCurve: Incomplete
    compatibilityReporterClass = SegmentCompatibilityReporter
    def isCompatible(self, other): ...
    def round(self) -> None: ...
