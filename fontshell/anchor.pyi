from _typeshed import Incomplete
from fontParts.base import BaseAnchor as BaseAnchor
from fontParts.fontshell.base import RBaseObject as RBaseObject

class RAnchor(RBaseObject, BaseAnchor):
    wrapClass: Incomplete
