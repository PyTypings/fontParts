from _typeshed import Incomplete
from fontParts.base import BaseComponent as BaseComponent
from fontParts.fontshell.base import RBaseObject as RBaseObject

class RComponent(RBaseObject, BaseComponent):
    wrapClass: Incomplete
