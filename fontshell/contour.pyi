from _typeshed import Incomplete
from fontParts.base import BaseContour as BaseContour
from fontParts.fontshell.bPoint import RBPoint as RBPoint
from fontParts.fontshell.base import RBaseObject as RBaseObject
from fontParts.fontshell.point import RPoint as RPoint
from fontParts.fontshell.segment import RSegment as RSegment

class RContour(RBaseObject, BaseContour):
    wrapClass: Incomplete
    pointClass = RPoint
    segmentClass = RSegment
    bPointClass = RBPoint
