from _typeshed import Incomplete
from fontParts.base import BaseFeatures as BaseFeatures
from fontParts.fontshell.base import RBaseObject as RBaseObject

class RFeatures(RBaseObject, BaseFeatures):
    wrapClass: Incomplete
