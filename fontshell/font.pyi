from _typeshed import Incomplete
from fontParts.base import BaseFont as BaseFont
from fontParts.fontshell.base import RBaseObject as RBaseObject
from fontParts.fontshell.features import RFeatures as RFeatures
from fontParts.fontshell.glyph import RGlyph
from fontParts.fontshell.groups import RGroups as RGroups
from fontParts.fontshell.guideline import RGuideline as RGuideline
from fontParts.fontshell.info import RInfo as RInfo
from fontParts.fontshell.kerning import RKerning as RKerning
from fontParts.fontshell.layer import RLayer as RLayer
from fontParts.fontshell.lib import RLib as RLib

class RFont(RBaseObject, BaseFont):
    wrapClass: Incomplete
    infoClass = RInfo
    groupsClass = RGroups
    kerningClass = RKerning
    featuresClass = RFeatures
    libClass = RLib
    layerClass = RLayer
    guidelineClass = RGuideline

    def newGlyph(self, name: str, clear: bool = ...) -> RGlyph: ...
