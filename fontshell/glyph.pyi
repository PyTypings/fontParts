from _typeshed import Incomplete
from fontParts.base import BaseGlyph as BaseGlyph
from fontParts.base.errors import FontPartsError as FontPartsError
from fontParts.fontshell.anchor import RAnchor as RAnchor
from fontParts.fontshell.base import RBaseObject as RBaseObject
from fontParts.fontshell.component import RComponent as RComponent
from fontParts.fontshell.contour import RContour as RContour
from fontParts.fontshell.guideline import RGuideline as RGuideline
from fontParts.fontshell.image import RImage as RImage
from fontParts.fontshell.lib import RLib as RLib

class RGlyph(RBaseObject, BaseGlyph):
    wrapClass: Incomplete
    contourClass = RContour
    componentClass = RComponent
    anchorClass = RAnchor
    guidelineClass = RGuideline
    imageClass = RImage
    libClass = RLib
    def getPen(self): ...
    def getPointPen(self): ...
