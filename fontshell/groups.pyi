from _typeshed import Incomplete
from fontParts.base import BaseGroups as BaseGroups
from fontParts.fontshell.base import RBaseObject as RBaseObject

class RGroups(RBaseObject, BaseGroups):
    wrapClass: Incomplete
