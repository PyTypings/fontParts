from _typeshed import Incomplete
from fontParts.base import BaseGuideline as BaseGuideline
from fontParts.fontshell.base import RBaseObject as RBaseObject

class RGuideline(RBaseObject, BaseGuideline):
    wrapClass: Incomplete
