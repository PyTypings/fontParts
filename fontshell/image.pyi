from _typeshed import Incomplete
from fontParts.base import BaseImage as BaseImage, FontPartsError as FontPartsError
from fontParts.fontshell.base import RBaseObject as RBaseObject

class RImage(RBaseObject, BaseImage):
    wrapClass: Incomplete
