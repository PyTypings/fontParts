from _typeshed import Incomplete
from fontParts.base import BaseInfo as BaseInfo
from fontParts.fontshell.base import RBaseObject as RBaseObject

class RInfo(RBaseObject, BaseInfo):
    wrapClass: Incomplete
