from _typeshed import Incomplete
from fontParts.base import BaseKerning as BaseKerning
from fontParts.fontshell.base import RBaseObject as RBaseObject

class RKerning(RBaseObject, BaseKerning):
    wrapClass: Incomplete
