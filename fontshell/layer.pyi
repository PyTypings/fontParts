from _typeshed import Incomplete
from fontParts.base import BaseLayer as BaseLayer
from fontParts.fontshell.base import RBaseObject as RBaseObject
from fontParts.fontshell.glyph import RGlyph as RGlyph
from fontParts.fontshell.lib import RLib as RLib

class RLayer(RBaseObject, BaseLayer):
    wrapClass: Incomplete
    libClass = RLib
    glyphClass = RGlyph
    lib: RLib
    tempLib: RLib

    def newGlyph(self, name: str, clear: bool = ...) -> RGlyph: ...
