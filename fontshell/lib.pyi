from _typeshed import Incomplete
from fontParts.base import BaseLib as BaseLib
from fontParts.fontshell.base import RBaseObject as RBaseObject

class RLib(RBaseObject, BaseLib):
    wrapClass: Incomplete
