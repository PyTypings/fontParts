from _typeshed import Incomplete
from fontParts.base import BasePoint as BasePoint, FontPartsError as FontPartsError
from fontParts.fontshell.base import RBaseObject as RBaseObject

class RPoint(RBaseObject, BasePoint):
    wrapClass: Incomplete
    def changed(self) -> None: ...
