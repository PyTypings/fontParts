from fontParts.base import BaseSegment as BaseSegment
from fontParts.fontshell.base import RBaseObject as RBaseObject

class RSegment(BaseSegment, RBaseObject): ...
